#include <iostream>
#include <stdlib.h>
#include "../src/DynamicPartitionBloomFilter.h"

using namespace std;
int main() {

	DynamicPartitionBloomFilter *a;
	// The parameters m should be calculated using the other 3 parameters for DPBF to work (maintain fp-rate under threshold)
	a = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);	// (m, k, D, fp)

	// Insert elements
	a->dpbf_insert(5);
	a->dpbf_insert(6);

	// Lookup elements
	// dpbf_lookup() returns true or false depending
	// on whether the element was present or not
	if(a->dpbf_lookup(5)) {
		cout << "5 found\n";
	}

	if(!a->dpbf_lookup(7)) {
		cout << "7 not found\n";
	}

	// Finally free the structure
	a->dpbf_free();

	return 0;
}