#include <iostream>
#include <stdlib.h>
#include "../src/DynamicPartitionBloomFilter.h"

using namespace std;
int main() {

	DynamicPartitionBloomFilter *a, *b, *c;
	// The parameters m should be calculated using the other 3 parameters for DPBF to work (maintain fp-rate under threshold)
	a = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);	// (m, k, D, fp)
	b = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);	
	c = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);

	// Insert an element
	a->dpbf_insert(5);
	b->dpbf_insert(5);
	b->dpbf_insert(6);

	// Taking intersection
	*c = *a & *b;

	if(c->dpbf_lookup(5)) {
		cout << "Common element 5 found in intersection\n";
	}

	if(!c->dpbf_lookup(6)) {
		cout << "6 not found in intersection\n";
	}

	// Free DPBFs
	a->dpbf_free();
	b->dpbf_free();
	c->dpbf_free();

	return 0;
}