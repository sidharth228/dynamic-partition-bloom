set terminal pdf enhanced font 'Segoe UI,20'

set grid y
set format y "10^{%L}"
set xtics (24, 26, 28, 30, 32, 34)

set key box inside width 2 font ",15"

##### construction time

set logscale y
set output '../out/plots/parameter_construction_time.pdf'
set key bottom right
set ylabel 'Construction Time (in s)'
set xlabel 'd'

plot '../out/parameter_selection_10000.txt' using 1:($2 + $3 + $4) title '10^4' with linespoints lw 2, '../out/parameter_selection_100000.txt' using 1:($2 + $3 + $4) title '10^5' with linespoints lw 2, '../out/parameter_selection_1000000.txt' using 1:($2 + $3 + $4) title '10^6' with linespoints lw 2 

##### Query Time

unset logscale y
unset format y
set output '../out/plots/parameter_query.pdf'
set key top left
set ylabel 'Query Time (in s)'
set xlabel 'd'

plot '../out/parameter_selection_10000.txt' using 1:5 title '10^4' with linespoints lw 2, '../out/parameter_selection_100000.txt' using 1:5 title '10^5' with linespoints lw 2, '../out/parameter_selection_1000000.txt' using 1:5 title '10^6' with linespoints lw 2 


##### FP Rate

set logscale y
set format y "10^{%L}"
set output '../out/plots/parameter_fp.pdf'
set key bottom right
set ylabel 'EFPR'
set xlabel 'd'

plot '../out/parameter_selection_10000.txt' using 1:6 title '10^4' with linespoints lw 2, '../out/parameter_selection_100000.txt' using 1:6 title '10^5' with linespoints lw 2, '../out/parameter_selection_1000000.txt' using 1:6 title '10^6' with linespoints lw 2 , [24:34] 0.01 title 'f_t' lt 2 lc rgb 'red'

##### Memory 

set logscale y
set format y "10^{%L}"
set output '../out/plots/parameter_memory_total.pdf'
set key bottom left
set ylabel 'Memory'
set xlabel 'd'

plot '../out/parameter_selection_10000.txt' using 1:9 title '10^4' with linespoints lw 2, '../out/parameter_selection_100000.txt' using 1:9 title '10^5' with linespoints lw 2, '../out/parameter_selection_1000000.txt' using 1:9 title '10^6' with linespoints lw 2 


set output '../out/plots/parameter_memory_snapshot.pdf'
set key bottom right
set ylabel 'Memory'
set xlabel 'd'

plot '../out/parameter_selection_10000.txt' using 1:7 title '10^4' with linespoints lw 2, '../out/parameter_selection_100000.txt' using 1:7 title '10^5' with linespoints lw 2, '../out/parameter_selection_1000000.txt' using 1:7 title '10^6' with linespoints lw 2 