set terminal pdf enhanced font 'Segoe UI,20'

set format y "10^{%L}"
set format x "10^{%L}"

set logscale xy
set grid y
set xtics (10,100,1000,10000,100000, 1000000)

##### construction time

set output '../out/plots/Construction_time_loglog.pdf'
set key outside vertical right
set ylabel 'Construction Time (in s)'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:($3 + $4) title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:($3 + $4) title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:($3 + $4 + $5) title 'DPBF' with linespoints lw 2

##### initialization time

set output '../out/plots/only_initialization.pdf'
set key outside vertical right
set ylabel 'Initialization Time (in s)'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:3 title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:3 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:3 title 'DPBF' with linespoints lw 2

##### insertion time

set output '../out/plots/only_insertion.pdf'
set key outside vertical right
set ylabel 'Insertion Time (in s)'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:4 title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:4 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:4 title 'DPBF' with linespoints lw 2


set key box inside width 2 font ",15"

##### Memory 

set output '../out/plots/Memory_loglog.pdf'
set key bottom right
set ylabel 'Memory'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:7 title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:7 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:10 title 'DPBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:8 title 'CPBPT' with linespoints lw 2

##### Query Time

set output '../out/plots/Query_time_loglog.pdf'
set key top left
set ylabel 'Query Time (in s)'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:5 title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:5 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:6 title 'DPBF' with linespoints lw 2, [10:2000000] 100 title '' lt 2 lc rgb 'white' lw 1

##### FP Rate

set output '../out/plots/FP_rate_loglog.pdf'
set key bottom left
set ylabel 'EFPR'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:6 title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:6 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:7 title 'DPBF' with linespoints lw 2, [10:2000000] 0.01 title 'f_t' lt 2 lc rgb 'red'

##### Ratio test

set output '../out/plots/ratio_comparison.pdf'
set key default
set key outside vertical right
set ylabel 'Query Time (in s)'
set xlabel 'Set size'
set title "Average time taken per query"

plot '../out/dpbf_ratio_001.txt' using 2:3 title '10^{-2}' with linespoints lw 2, '../out/dpbf_ratio_01.txt' using 2:3 title '10^{-1}' with linespoints lw 2, '../out/dpbf_ratio_1.txt' using 2:3 title '10^0' with linespoints lw 2, '../out/dpbf_ratio_10.txt' using 2:3 title '10^1' with linespoints lw 2, '../out/dpbf_ratio_100.txt' using 2:3 title '10^2' with linespoints lw 2, '../out/dpbf_ratio_optimal.txt' using 2:($6 / 1000000) title '10^{/Symbol \245}' with lines lt 3 lc rgb 'red'

##### Set operations

set key box inside width 2 font ",15"

##### Union

unset title

##### FP Rate

set output '../out/plots/Union_FP.pdf'
set key top left
set ylabel 'EFPR'
set xlabel 'Symmetric Set Difference'

plot '../out/union_sbf.txt' using 1:2 title 'SBF' with linespoints lw 2, '../out/union_dbf.txt' using 1:2 title 'DBF' with linespoints lw 2, '../out/union_dpbf.txt' using 1:2 title 'DPBF' with linespoints lw 2, [1000:100000] 0.01 title 'f_t' lt 2 lc rgb 'red', [1000:100000] 2000 title '' lt 2 lc rgb 'white' lw 1

##### Memory 

set output '../out/plots/Union_memory.pdf'
set key top left
set ylabel 'Memory'
set xlabel 'Symmetric Set Difference'

plot '../out/union_sbf.txt' using 1:3 title 'SBF' with linespoints lw 2, '../out/union_dbf.txt' using 1:3 title 'DBF' with linespoints lw 2, '../out/union_dpbf.txt' using 1:3 title 'DPBF' with linespoints lw 1, '../out/union_dpbf.txt' using 1:5 title 'CPBPT' with linespoints lw 2

##### Intersection
##### FP Rate

set output '../out/plots/Intersection_FP.pdf'
set key bottom right
set ylabel 'EFPR'
set xlabel 'Symmetric Set Difference'

plot '../out/intersection_sbf.txt' using 1:2 title 'SBF' with linespoints lw 2, '../out/intersection_dbf.txt' using 1:2 title 'DBF' with linespoints lw 2, '../out/intersection_dpbf.txt' using 1:2 title 'DPBF' with linespoints lw 2, [10:2000000] 0.01 title 'f_t' lt 2 lc rgb 'red'

##### Memory 

set output '../out/plots/Intersection_memory.pdf'
set key top left
set ylabel 'Memory'
set xlabel 'Symmetric Set Difference'

plot '../out/intersection_sbf.txt' using 1:3 title 'SBF' with linespoints lw 2, '../out/intersection_dbf.txt' using 1:3 title 'DBF' with linespoints lw 2, '../out/intersection_dpbf.txt' using 1:3 title 'DPBF' with linespoints lw 1, '../out/intersection_dpbf.txt' using 1:5 title 'CPBPT' with linespoints lw 2, [1000:100000] 10000 title '' lt 2 lc rgb 'white' lw 1

##### No SBF comparisons

set key default

##### Query Time

set output '../out/plots/NoSBFQuery_time_loglog.pdf'
set key outside vertical right
set ylabel 'Query Time (in s)'
set xlabel 'Set size'
set title "Time taken for 10^6 membership queries"

plot '../out/bt_dbf.txt' using 2:5 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:6 title 'DPBF' with linespoints lw 2

##### FP Rate

set output '../out/plots/NoSBFFP_rate_loglog.pdf'
set key outside vertical right
set ylabel 'EFPR'
set xlabel 'Set size'
set title "Empirical False Positive Rate (EFPR)"

plot '../out/bt_dbf.txt' using 2:6 title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:7 title 'DPBF' with linespoints lw 2, [10:2000000] 0.01 title 'f_t' lt 2 lc rgb 'red'