set terminal pdf enhanced font 'Segoe UI,20'

set format x "10^{%L}"
set key font ",19"

set logscale x
set grid y
set xtics (10,100,1000,10000,100000, 1000000)

##### Memory Ratio

set output '../out/plots/mem_ratio_c4.pdf'
set key outside vertical right
set ylabel 'Compression'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C4_D25.txt' using 2:3 title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D26.txt' using 2:3 title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D27.txt' using 2:3 title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D28.txt' using 2:3 title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D29.txt' using 2:3 title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D30.txt' using 2:3 title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D31.txt' using 2:3 title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D32.txt' using 2:3 title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D33.txt' using 2:3 title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D34.txt' using 2:3 title '34' with lines lw 1


set output '../out/plots/mem_ratio_c6.pdf'
set key outside vertical right
set ylabel 'Compression'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C6_D25.txt' using 2:3 title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D26.txt' using 2:3 title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D27.txt' using 2:3 title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D28.txt' using 2:3 title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D29.txt' using 2:3 title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D30.txt' using 2:3 title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D31.txt' using 2:3 title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D32.txt' using 2:3 title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D33.txt' using 2:3 title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D34.txt' using 2:3 title '34' with lines lw 1


set output '../out/plots/mem_ratio_c10.pdf'
set key outside vertical right
set ylabel 'Compression'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C10_D25.txt' using 2:3 title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D26.txt' using 2:3 title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D27.txt' using 2:3 title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D28.txt' using 2:3 title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D29.txt' using 2:3 title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D30.txt' using 2:3 title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D31.txt' using 2:3 title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D32.txt' using 2:3 title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D33.txt' using 2:3 title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D34.txt' using 2:3 title '34' with lines lw 1


##### n_t Ratio

set output '../out/plots/num_ratio_c4.pdf'
set key outside vertical right
set ylabel '% of uBF filled'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C4_D25.txt' using 2:4 title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D26.txt' using 2:($4 * 100) title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D27.txt' using 2:($4 * 100) title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D28.txt' using 2:($4 * 100) title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D29.txt' using 2:($4 * 100) title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D30.txt' using 2:($4 * 100) title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D31.txt' using 2:($4 * 100) title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D32.txt' using 2:($4 * 100) title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D33.txt' using 2:($4 * 100) title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C4_D34.txt' using 2:($4 * 100) title '34' with lines lw 1


set output '../out/plots/num_ratio_c6.pdf'
set key outside vertical right
set ylabel '% of uBF filled'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C6_D25.txt' using 2:($4 * 100) title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D26.txt' using 2:($4 * 100) title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D27.txt' using 2:($4 * 100) title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D28.txt' using 2:($4 * 100) title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D29.txt' using 2:($4 * 100) title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D30.txt' using 2:($4 * 100) title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D31.txt' using 2:($4 * 100) title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D32.txt' using 2:($4 * 100) title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D33.txt' using 2:($4 * 100) title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C6_D34.txt' using 2:($4 * 100) title '34' with lines lw 1


set output '../out/plots/num_ratio_c10.pdf'
set key outside vertical right
set ylabel '% of uBF filled'
set xlabel 'Set size'

plot '../out/mem_ratio_results/compression_ratio_C10_D25.txt' using 2:($4 * 100) title '25' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D26.txt' using 2:($4 * 100) title '26' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D27.txt' using 2:($4 * 100) title '27' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D28.txt' using 2:($4 * 100) title '28' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D29.txt' using 2:($4 * 100) title '29' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D30.txt' using 2:($4 * 100) title '30' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D31.txt' using 2:($4 * 100) title '31' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D32.txt' using 2:($4 * 100) title '32' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D33.txt' using 2:($4 * 100) title '33' with lines lw 1, '../out/mem_ratio_results/compression_ratio_C10_D34.txt' using 2:($4 * 100) title '34' with lines lw 1


##### c_f ratio

unset logscale x
set logscale xy
set format y "10^{%L}"
set key font ",20"

set output '../out/plots/NoSBF_c_f_ratio.pdf'
set key outside vertical right
set ylabel 'C/f'
set xlabel 'Set size'

plot '../out/bt_dbf.txt' using 2:((($2 * 8)/$7) / ($6)) title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:((($2 * 8)/$8) / ($7)) title 'DPBF' with linespoints lw 2

set key box inside width 2 font ",15"

set output '../out/plots/c_f_ratio.pdf'
set key top left
set ylabel 'C/f'
set xlabel 'Set size'

plot '../out/bt_sbf.txt' using 2:((($2 * 8)/$7) / ($6)) title 'SBF' with linespoints lw 2, '../out/bt_dbf.txt' using 2:((($2 * 8)/$7) / ($6)) title 'DBF' with linespoints lw 2, '../out/bt_dpbf.txt' using 2:((($2 * 8)/$8) / ($7)) title 'DPBF' with linespoints lw 2
