#ifndef _DYNAMIC_BLOOM_FILTER_H_
#define _DYNAMIC_BLOOM_FILTER_H_

#include <iostream>
#include <cmath>
#include <vector>

#include "StandardBloomFilter.h"

/***
	Class for DBF. Must have the following functions -

	Insertion and lookup : 
		-	dbf_insert(uint64_t val)
		-	dbf_lookup(uint64_t val)

	Union and intersection operators are defined accordingly.
***/

class DynamicBloomFilter {
	public:
		double fp;              		// false positive rate threshold
		uint32_t sbf_num_bits;			// m
		uint32_t sbf_num_hashes;		// k
		uint32_t sbf_capacity;			// max elements to be stored in a single SBF
		uint32_t num_active_elements;	// total number of elements stored in the currently "active" SBF
		std::vector <StandardBloomFilter> sbf_vector; 
		
		DynamicBloomFilter(double, uint32_t, uint32_t);

		void dbf_insert(uint64_t);
        bool dbf_lookup(uint64_t);         

        DynamicBloomFilter operator|(DynamicBloomFilter& dbf);
        DynamicBloomFilter operator&(DynamicBloomFilter& dbf);		        
};

/**
	Constructor for DBF class.
	@param fp 			 	: The FP threshold to be maintained
	@param sbf_num_bits  	: The size of SBF in bits
	@param sbf_num_hashes 	: The number of hash functions used by SBF
**/
DynamicBloomFilter::DynamicBloomFilter(double fp, uint32_t sbf_num_bits, uint32_t sbf_num_hashes) {
	this->fp = fp;	
	this->sbf_num_bits = sbf_num_bits;
	this->sbf_num_hashes = sbf_num_hashes;
	this->sbf_capacity = static_cast<uint32_t>((-static_cast<float>(sbf_num_bits) / sbf_num_hashes) * log(1 - exp(log(fp) / sbf_num_hashes)));
	this->num_active_elements = 0;
	this->sbf_vector.push_back(StandardBloomFilter(fp, sbf_num_bits, sbf_num_hashes));
}

/**
	Function to insert in DBF.
	@param x : The value to be inserted
**/
void DynamicBloomFilter::dbf_insert(uint64_t x) {
	if(this->num_active_elements >= this->sbf_capacity) {
		sbf_vector.push_back(StandardBloomFilter(fp, sbf_num_bits, sbf_num_hashes));
		this->num_active_elements = 0;
	}

	sbf_vector.at(sbf_vector.size() - 1).sbf_insert(x);
	num_active_elements++;
}

/**
	Function to check if an element is present in DBF. 
	@param x : The value to be checked
**/
bool DynamicBloomFilter::dbf_lookup(uint64_t x) {
	for(uint32_t i = 0; i < sbf_vector.size(); i++) {
		if(sbf_vector.at(i).sbf_lookup(x)) {
			return true;
		} 
	}
	return false;
}

/**
	Union operator for DBF
**/
DynamicBloomFilter DynamicBloomFilter::operator| (DynamicBloomFilter& dbf) {
	assert(this->fp == dbf.fp);
	assert(this->sbf_num_hashes == dbf.sbf_num_hashes);
	assert(this->sbf_num_bits == dbf.sbf_num_bits);
	assert(this->sbf_capacity == dbf.sbf_capacity);

	DynamicBloomFilter ret(this->fp, this->sbf_num_bits, this->sbf_num_hashes);
	ret.num_active_elements = this->sbf_capacity;
	ret.sbf_vector = this->sbf_vector;
	ret.sbf_vector.insert(ret.sbf_vector.end(), dbf.sbf_vector.begin(), dbf.sbf_vector.end());
	return ret;
}

/**
	Intersection operator for DBF
**/
DynamicBloomFilter DynamicBloomFilter::operator& (DynamicBloomFilter& dbf) {
	assert(this->fp == dbf.fp);
	assert(this->sbf_num_hashes == dbf.sbf_num_hashes);
	assert(this->sbf_num_bits == dbf.sbf_num_bits);
	assert(this->sbf_capacity == dbf.sbf_capacity);

	DynamicBloomFilter ret(this->fp, this->sbf_num_bits, this->sbf_num_hashes);
	ret.num_active_elements = this->sbf_capacity;

	for(uint32_t i = 0; i < this->sbf_vector.size(); i++) {
		for(uint32_t j = 0; j < dbf.sbf_vector.size(); j++) {
			ret.sbf_vector.push_back(this->sbf_vector[i] & dbf.sbf_vector[j]);
		}
	}
	return ret;
}

#endif