#ifndef _STANDARD_BLOOM_FILTER_H
#define _STANDARD_BLOOM_FILTER_H

#include <boost/dynamic_bitset.hpp>
#include <cmath>
#include <functional>
#include <iostream>
#include <vector>
#include "MurmurHash3.cpp"

using namespace std;

/***
    Class for a standard bloom filter (SBF). Must define the following functions - 

    Insertion and lookup :
        -   sbf_insert(uint64_t val)
        -   sbf_lookup(uint64_t val)
    
    Union and intersection operators are defined as usual.
***/

class StandardBloomFilter {
public:
    static double fp;                                   // false positive rate threshold
    static uint32_t bloom_size;                         // m (in bits)
    static uint32_t num_hash;                           // k
    uint32_t num_elements;

    boost::dynamic_bitset<uint8_t> bit_vector;         // underlying bit_vector (composed of blocks of uint8_t)

    StandardBloomFilter(double, uint32_t, uint32_t);

    void sbf_insert(uint64_t);
    bool sbf_lookup(uint64_t);
    bool lookup_insert_helper(uint64_t, bool);
    uint32_t estimated_population();
    void copy_sbf(StandardBloomFilter*);

    StandardBloomFilter operator|(StandardBloomFilter& sbf);
    StandardBloomFilter operator&(StandardBloomFilter& sbf);
    void operator|=(StandardBloomFilter& sbf);
    void operator&=(StandardBloomFilter& sbf);

    void bit_vector_properties();
};

double StandardBloomFilter::fp = 0;
uint32_t StandardBloomFilter::bloom_size = 0;
uint32_t StandardBloomFilter::num_hash = 0;

/**
    The constructor of our SBF class.
    @param fp           : The FP-threshold for SBF
    @param bloom_size   : The size of SBF in bits 
    @param num_hash     : The number of hash functions used by SBF
**/
StandardBloomFilter::StandardBloomFilter(double fp, uint32_t bloom_size, uint32_t num_hash) {
    this->bit_vector = boost::dynamic_bitset<uint8_t>(bloom_size); // all 0's by default
    this->num_elements = 0;
    this->fp = fp;
    this->bloom_size = bloom_size;
    this->num_hash = num_hash;
}

/**
    A helper function used for insert and lookup. 
    @param x        : The value to be inserted/looked-up
    @param insert   : The case defining whether it's an insert or a lookup (true in case of insert)
    @return bool    : True or False depending on whether x was present or not in case of look-up
**/
bool StandardBloomFilter::lookup_insert_helper(uint64_t x, bool insert) {
    uint64_t hash_digests[2];
    uint32_t hits = 0;

    // store the 128 bit output in two 64 bit variables
    MurmurHash3_x64_128(&x, 8, 0, hash_digests);

    for(uint64_t i = 0; i < this->num_hash; i++) {
        uint32_t pos = (hash_digests[0] % this->bloom_size + i*(hash_digests[1] % this->bloom_size)) % this->bloom_size;
        if(this->bit_vector[pos] == 1) {
            hits++;
        } else if(insert) {
            this->bit_vector[pos] = 1;
        } else {
            return false;
        }
    }

    if(insert && hits < this->num_hash) {
        this->num_elements = this->num_elements + 1;
    } else if (hits == this->num_hash) {
        return true;
    }
}

/**
    Function to insert in SBF.
    @param x : Value to be inserted in SBF
**/
void StandardBloomFilter::sbf_insert(uint64_t x) {
    this->lookup_insert_helper(x, true);
}

/**
    Function to check if an element is in SBF
    @param x : Value to be checked in SBF
**/
bool StandardBloomFilter::sbf_lookup(uint64_t x) {
    return this->lookup_insert_helper(x, false);
}

/**
    Union operator for SBF. 
**/
StandardBloomFilter StandardBloomFilter::operator|(StandardBloomFilter& sbf) {
    StandardBloomFilter ret(this->fp, this->bloom_size, this->num_hash);
    ret.bit_vector = this->bit_vector | sbf.bit_vector;
    ret.num_elements = estimated_population();
    return ret;
}

/**
    Intersection operator for SBF. 
**/
StandardBloomFilter StandardBloomFilter::operator&(StandardBloomFilter& sbf) {
    StandardBloomFilter ret(this->fp, this->bloom_size, this->num_hash);
    ret.bit_vector = this->bit_vector & sbf.bit_vector;
    ret.num_elements = estimated_population();
    return ret;
}

/**
    Another union operator.
**/
void StandardBloomFilter::operator|=(StandardBloomFilter& sbf) {
    this->bit_vector |= sbf.bit_vector;
    this->num_elements = estimated_population();
}

/**
    Another intersection operator.
**/
void StandardBloomFilter::operator&=(StandardBloomFilter& sbf) {
   this->bit_vector &= sbf.bit_vector;
   this->num_elements = estimated_population();
}

/**
    Function to estimate the number of elements in an SBF based on the number of bits set. It is used in
    case we are taking union/intersection of two SBF's.
**/
uint32_t StandardBloomFilter::estimated_population() {
    uint32_t ret;
    double estimated_count = (1.0 * this->bloom_size) / this->num_hash;
    estimated_count *= -1;
    estimated_count *= log(1.0 - (1.0 * this->bit_vector.count()) / this->bloom_size);
    ret = ceil(estimated_count);
    return ret;
}

/**
    Debugging function to print information about an SBF.
**/
void StandardBloomFilter::bit_vector_properties() {
    std::cout << "bit_vector: " << this->bit_vector << std::endl;
    std::cout << "lsb to msb: ";
    for (boost::dynamic_bitset<>::size_type i = 0; i < this->bit_vector.size(); ++i)
        std::cout << this->bit_vector[i];
    std::cout << std::endl;
    std::cout << "total number of bits: " << this->bit_vector.size() << std::endl;
    std::cout << "number of set bits: " << this->bit_vector.count() << std::endl;
    std::cout << "bits per block: " << this->bit_vector.bits_per_block << std::endl;
    std::cout << "num_blocks: " << this->bit_vector.num_blocks() << std::endl << std::endl;
}

/**
    Function to make a copy of SBF. Maybe not required.    
 **/
void StandardBloomFilter::copy_sbf(StandardBloomFilter *a) {
    for(uint32_t i = 0; i < this->bloom_size; i++) {
        this->bit_vector[i] = a->bit_vector[i];
    }
    this->num_elements = a->num_elements;
}

#endif
