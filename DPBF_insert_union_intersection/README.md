# Dynamic Partition Bloom Filters - *Insert-Union-Intersection*

This README explains how to use the code for DPBF to insert elements and lookup as well as how to perform union and intersection operations using two DPBFs. This assumes that you've already downloaded the required code on your local system and imported the required libraries.

## Initializing

First you must declare the struture and initialize it using the constructor. 
```
DynamicPartionBloomFilter *a;
a = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);	// (m, k, D, n_t, p)
```
The constructor expects 5 arguments : 

* m : This is the size of the Standard Bloom filters used in DPBF in bits. 

* k : This is the number of hash functions to be used for Standard bloom filters. 

* D : This is the maximum possible height of CPBPT.

* n_t : This is the maximum number of elements each individual uBF can store, to maintain the overall FP-Rate.  

* p : This is the FP-threshold we wish to maintain.

All parameters are not independent and some of them must be calculated. The user decides on a given p, k and D. n_t is calculated using the following formula : 

![alt text](util/images/n_t_equation.png?raw=true "Equation for calculating n_t")

m is calculated using the formula : 

![alt text](util/images/bloom_size_equation.png?raw=true "Equation for calculating m")

D is selected based on the kind of application the user is using the structure in and whether he requires better query times or better memory. (See the paper for more details)

## Insertion

To insert an element e, we use the following function 
```
a->dpbf_insert(e);
```
Here the element (e) inserted must be an integer with atmost 64 bits.

## Query

To check if an element e exists in the structure, we use the following function
```
a->dpbf_query(e);
```
This returns true or false based on whether the element was present or not.

## Set Operations
Suppose *a* and *b* are two DPBFs initialized with the same parameters as follows 
```
DynamicPartitionBloomFilter *a = new DynamicPartitionBloomfilter(m, k, D, n_t, p);
DynamicPartitionBloomFilter *b = new DynamicPartitionBloomfilter(m, k, D, n_t, p);
```

Suppose they are now populated with some elements.

### Union
To take their union, we first declare a third DPBF with the same parameters as follows
```
DynamicPartitionBloomFilter *c = new DynamicPartitionBloomfilter(m, k, D, n_t, p);
```
We then take the union as follows 
```
*c = *a | *b
```
The | operator expects two DPBFs and returns the DPBF containing their union. We also hae the |= operator for inplace union.

### Intersection
To take their intersection, we first declare a third DPBF with the same parameters as follows
```
DynamicPartitionBloomFilter *c = new DynamicPartitionBloomfilter(m, k, D, n_t, p);
```
We then take the intersection as follows 
```
*c = *a & *b
```
The & operator expects two DPBFs and returns the DPBF containing their union. We also have the &= operator for inplace intersection.

## DBF and SBF

In case you want to use DBF or SBF, the method is the same. First you must initialize the structure using their constructor. The insert and lookup functions are changed to sbf_insert(), sbf_lookup(), dbf_insert(), dbf_lookup() respectively. An example is provided below 
```
\\ DBF Example
DynamicBloomFilter *a = new DynamicBloomFilter(0.01, 170, 3);	// (p, m, k)
a->dbf_insert(5);
a->dbf_lookup(5);

\\SBF Example
StandardBloomFilter *b = new StandardBloomFilter(0.01, 170, 3);	// (p, m, k)
b->sbf_insert(6);
b->sbf_lookup(6);
```

## Tests

The test folder contains a file test_functionc.c which contains all the test functions used to analyze our structure. To run the tests, you must first download the dataset used for testing which is available [here](https://drive.google.com/drive/folders/1p5X4Mwxb3tduQ1UE1F93t5kcV4d_Xep4?usp=sharing). Then in the main() function of test_functions.c, you must change the path to the dataset as follows : 
```
uint64_t* userIDs = read_data_from_file("path/to/dataset/unique_userIDs.txt", 3008497);
uint64_t* reduced_namespace = read_data_from_file("path/to/dataset/random_sample_new.txt", 1e8);
```

To run any test, simply uncomment it from the main() function. Make test_functions.out using the Makefile and run the test using the command - 
```
./test_functions.out
```
The names of output files can be changed in the main() function itself. 

To make plots using the output files generated, you can use the .gnu scripts provided in the util folder. Simply run them using the following command 
```
gnuplot <script_name>.gnu
```
The plots will be generated in the plots folder in out folder.

## Partition Hash Function
In case you want to use your own partition hash function, you must change the partition_hash.h file present in src folder. The name and parameters of partition_hash() function must remain unchanged. An example would be 
```
inline uint32_t partition_hash(uint64_t val, uint32_t level) {
\\ your own partition hash function
}
```

## Hash Function
For Standard Bloom Filters, we are currently using MurmurHash3 function. In case you want to use your own hash function, you must change lookup_insert_helper() function in StandardBloomFilter.h present in src folder. 
