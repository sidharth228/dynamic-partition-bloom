#include <iostream>
#include <fstream>
#include <ctime>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <sstream>
#include "../src/DynamicPartitionBloomFilter.h"
#include "../src/StandardBloomFilter.h"
#include "../src/DynamicBloomFilter.h"

using namespace std;

string BASIC_TESTS_DPBF_FILENAME, BASIC_TESTS_SBF_FILENAME, BASIC_TESTS_DBF_FILENAME, UNION_SBF_FILENAME, UNION_DBF_FILENAME, UNION_DPBF_FILENAME, INTERSECTION_SBF_FILENAME, INTERSECTION_DBF_FILENAME, INTERSECTION_DPBF_FILENAME;

// Reads twitter data and returns an array storing the same
uint64_t* read_data_from_file(string filename, uint32_t sample_size)
{
	uint64_t *selected = (uint64_t *)malloc(sample_size*sizeof(uint64_t));
	FILE *reader = fopen(filename.c_str(), "r");
	for(uint32_t i = 0; i < sample_size; i++) {
		int scanf_val = fscanf(reader, "%lu\n", &selected[i]);
	}

	fclose(reader);
	return selected;
}

void basic_tests_dpbf(uint64_t *userIDs, uint64_t *reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t num_partition, uint32_t fill_threshold, uint32_t bloom_size, double fp_threshold) {
	uint32_t plot_points[18] = {0,10,20,60,100,200,600,1000,2000,6000,10000,20000,60000,100000,200000,600000,1000000,2000000};

	// DPBF parameters
	DynamicPartitionBloomFilter *a;

	uint32_t query_set_size = 1e6;
	clock_t start, end;
	double construction_time, insertion_time, updation_time, query_time, false_positive_rate;
	uint64_t memory_snapshot, memory_map, memory_total;

	ofstream basic_tests_file;
	basic_tests_file.open(BASIC_TESTS_DPBF_FILENAME);

	for(uint32_t idx = 1; idx < 18; idx++) {
		cout<<"idx:"<<idx<<endl;
		uint32_t set_size = plot_points[idx];

		// Construction time
		start = clock();
		a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);
		end = clock();
		construction_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Insertion time
		start = clock();
		for(uint32_t i=0; i<set_size; i++) {
			a->dpbf_insert(userIDs[i]);
		}
		end = clock();
		insertion_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Updation time
		start = clock();
		a->snapshot = a->dpbf_node_update(a->snapshot, 0, 0);
		end = clock();
		updation_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Query time
		start = clock();
		for(uint32_t i=0; i<query_set_size; i++) {
			a->dpbf_lookup(reduced_namespace[i]);
		}
		end = clock();
		query_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Memory
		uint64_t size_non_leaf = sizeof(b_node);
		uint64_t size_leaf = sizeof(b_node) + a->bloom_size/8;
		uint64_t size_nodesUsed = a->myMap->size() * 4;
		memory_snapshot = count_non_leaves(a->snapshot) * size_non_leaf + count_leaves(a->snapshot) * size_leaf;
		memory_map = a->myMap->size() * (4 + a->bloom_size/8);			// size of key = 4, size of sbf = bloom_size/8
		memory_total = memory_map + memory_snapshot + size_nodesUsed;	// in bytes

		// False positive rate
		uint64_t positives = 0;
		for(uint64_t i=0; i<reduced_namespace_size; i++) {
			if(a->dpbf_lookup(reduced_namespace[i]))
				positives++;
		}
		false_positive_rate = (double)(positives - set_size)/reduced_namespace_size;

		basic_tests_file<<idx<<" "<<set_size<<" "<<construction_time<<" "<<insertion_time<<" "<<updation_time<<" "<<query_time<<" "<<false_positive_rate<<" "<<memory_snapshot<<" "<<memory_map<<" "<<memory_total<<endl;

		// Free Memory	
		a->dpbf_free();
	}
	basic_tests_file.close();
}

void basic_tests_sbf(uint64_t *userIDs, uint64_t *reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, double fp_threshold) {
	// Data parameters
	uint32_t plot_points[18] = {0,10,20,60,100,200,600,1000,2000,6000,10000,20000,60000,100000,200000,600000,1000000,2000000};

	// DPBF parameters
	StandardBloomFilter *a;

	uint32_t query_set_size = 1e6;
	double factor = -((double)k) / log(1.0 - pow(fp_threshold, 1.0/((double)k)));
	uint32_t bloom_size = factor * 2e6;

	clock_t start, end;
	double construction_time, insertion_time, query_time, false_positive_rate;
	uint64_t memory_total;

	ofstream basic_tests_file;
	basic_tests_file.open(BASIC_TESTS_SBF_FILENAME);

	for(uint32_t idx=1; idx < 18; idx++) {
		cout<<"idx:"<<idx<<endl;
		uint32_t set_size = plot_points[idx];

		// Construction time
		start = clock();
		a = new StandardBloomFilter(fp_threshold, bloom_size, k);
		end = clock();
		construction_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Insertion time
		start = clock();
		for(uint32_t i=0; i<set_size; i++) {
			a->sbf_insert(userIDs[i]);
		}
		end = clock();
		insertion_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Query time
		start = clock();
		for(uint32_t i=0; i<query_set_size; i++) {
			a->sbf_lookup(userIDs[i]);
		}
		end = clock();
		query_time = (double)(end - start)/CLOCKS_PER_SEC;
	
		// Memory
		memory_total = sizeof(StandardBloomFilter) + bloom_size / 8;					// in bytes

		// False positive rate
		uint64_t positives = 0;
		for(uint64_t i=0; i<reduced_namespace_size; i++) {
			if(a->sbf_lookup(reduced_namespace[i]))
				positives++;
		}
		false_positive_rate = (double)(positives - set_size)/reduced_namespace_size;

		basic_tests_file<<idx<<" "<<set_size<<" "<<construction_time<<" "<<insertion_time<<" "<<query_time<<" "<<false_positive_rate<<" "<<memory_total<<endl;
	}
	basic_tests_file.close();
}

void basic_tests_dbf(uint64_t *userIDs, uint64_t *reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t bloom_size, double fp_threshold) {
	// Data parameters
	uint32_t plot_points[18] = {0,10,20,60,100,200,600,1000,2000,6000,10000,20000,60000,100000,200000,600000,1000000,2000000};

	// DPBF parameters
	DynamicBloomFilter *a;

	uint32_t query_set_size = 1e6;
	clock_t start, end;
	double construction_time, insertion_time, query_time, false_positive_rate;
	uint64_t memory_total;

	ofstream basic_tests_file;
	basic_tests_file.open(BASIC_TESTS_DBF_FILENAME);

	for(uint32_t idx=1; idx < 18; idx++) {
		cout<<"idx:"<<idx<<endl;
		uint32_t set_size = plot_points[idx];

		// Construction time
		start = clock();
		a = new DynamicBloomFilter(fp_threshold, bloom_size, k);
		end = clock();
		construction_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Insertion time
		start = clock();
		for(uint32_t i=0; i<set_size; i++) {
			a->dbf_insert(userIDs[i]);
		}
		end = clock();
		insertion_time = (double)(end - start)/CLOCKS_PER_SEC;

		// Query time
		start = clock();
		for(uint32_t i=0; i<query_set_size; i++) {
			a->dbf_lookup(userIDs[i]);
		}
		end = clock();
		query_time = (double)(end - start)/CLOCKS_PER_SEC;
	
		// Memory
		memory_total = a->sbf_vector.size() * (a->sbf_num_bits/8) + sizeof(DynamicBloomFilter);			// in bytes

		// False positive rate
		uint64_t positives = 0;
		for(uint64_t i=0; i<reduced_namespace_size; i++) {
			if(a->dbf_lookup(reduced_namespace[i]))
				positives++;
		}
		false_positive_rate = (double)(positives - set_size)/reduced_namespace_size;

		basic_tests_file<<idx<<" "<<set_size<<" "<<construction_time<<" "<<insertion_time<<" "<<query_time<<" "<<false_positive_rate<<" "<<memory_total<<endl;
	}
	basic_tests_file.close();
}

void dpbf_ratio_test(uint32_t k, uint32_t num_partition, uint32_t fill_threshold, uint32_t bloom_size, double fp_threshold) {
	// DPBF parameters
	DynamicPartitionBloomFilter *a;

	ofstream basic_tests_file;
	int plot_points[11] = {1000,2000,6000,10000,20000,60000,100000,200000,600000,1000000,2000000};	
	uint32_t insert_queries = 100, membership_queries, num_iterations;
	uint64_t UNIVERSE_SIZE = 10000000000;
	clock_t start, end;
	double query_time;

	string ratio_file_names[] = {"../out/dpbf_ratio_001.txt",
								 "../out/dpbf_ratio_01.txt",
								 "../out/dpbf_ratio_1.txt",
								 "../out/dpbf_ratio_10.txt",
								 "../out/dpbf_ratio_100.txt"};

	double ratios[] = {0.01, 0.1, 1, 10, 100};

	for(uint32_t j = 0; j < 5; j++) {
		double ratio = ratios[j];
		basic_tests_file.open(ratio_file_names[j]);
		cout<<"Ratio:"<<ratio<<endl;
		membership_queries = insert_queries * ratio;
		cout<<"Insert queries:"<<insert_queries<<"\nMembership queries:"<<membership_queries<<endl;
		
		for(uint32_t set_index = 0; set_index < 11; set_index++) {
			uint32_t SET_SIZE = plot_points[set_index];
			cout<<"Set size:"<<SET_SIZE<<endl;

			//Initialize
			a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);
			num_iterations = SET_SIZE / insert_queries;
			query_time = 0;

			for(uint32_t i = 0; i < num_iterations; i++) {
				//Insert queries
				for(uint32_t insert_i = 0; insert_i < insert_queries; insert_i++) {
					a->dpbf_insert(rand() % UNIVERSE_SIZE);
				}

				//Membership queries
				start = clock();
				for(uint32_t query_i = 0; query_i < membership_queries; query_i++) {
					a->dpbf_lookup(rand() % UNIVERSE_SIZE);
				}
				end = clock();

				query_time += (double)(end - start) / (CLOCKS_PER_SEC);
			}

			//Taking average
			query_time = query_time / (membership_queries * num_iterations);

			basic_tests_file<<ratio<<" "<<SET_SIZE<<" "<<query_time<<endl;

			//Free Memory
			a->dpbf_free();
		}
		basic_tests_file.close();
	}
}

void union_test_sbf(double fp_threshold, uint64_t *reduced_namespace, uint32_t reduced_namespace_size, uint32_t k) {
	ofstream basic_tests_file;
	basic_tests_file.open(UNION_SBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	uint32_t dpbf_mem[] = {237082, 269987, 408132, 551153, 918178, 2384685, 3787750};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	StandardBloomFilter *a, *b, *c;

	double factor = -((double)k) / log(1.0 - pow(fp_threshold, 1.0/((double)k)));
	uint32_t bloom_size = factor * 2e6;

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;
		bloom_size = dpbf_mem[idx-1];

		a = new StandardBloomFilter(fp_threshold, bloom_size, k);	
		b = new StandardBloomFilter(fp_threshold, bloom_size, k);

		// Insert common elements in both a and b
		for(uint32_t i=0; i < num_elements_in_intersection; i++) {
			a->sbf_insert(reduced_namespace[i]);
			b->sbf_insert(reduced_namespace[i]);
		}

		uint32_t set_size = plot_points[idx] / 2;
		double avg_union_fp = 0.0;

		for(uint32_t i = num_elements_in_intersection; i < num_elements_in_intersection + set_size; i++) {
			a->sbf_insert(reduced_namespace[i]);
		}

		for(uint32_t i = num_elements_in_intersection + set_size; i < num_elements_in_intersection + (2 * set_size); i++) {
			b->sbf_insert(reduced_namespace[i]);
		}

		//Find union
		c = new StandardBloomFilter(fp_threshold, bloom_size, k);
		(*c) = (*a) | (*b);

		// Calculate fp_rate
		uint64_t positives = 0;
		for(uint32_t i=0; i < reduced_namespace_size; i++) {
			if(c->sbf_lookup(reduced_namespace[i])) {
				positives++;
			}
		}

		cout<<"positives:"<<positives<<endl;
		avg_union_fp = (double)(positives - (plot_points[idx] + num_elements_in_intersection)) / (double)(reduced_namespace_size);
		cout<<"fp rate:"<<avg_union_fp<<endl;

		basic_tests_file<<plot_points[idx]<<" "<<avg_union_fp<<" "<<bloom_size<<endl;
		delete a;
		delete b;
		delete c;
	}
	basic_tests_file.close();
}

void intersection_test_sbf(double fp_threshold, uint64_t* reduced_namespace, uint32_t reduced_namespace_size, uint32_t k) {
	ofstream basic_tests_file;
	basic_tests_file.open(INTERSECTION_SBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	uint32_t dpbf_mem[] = {212125, 212125, 212305, 212485, 212856, 218862, 225942};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	StandardBloomFilter *a, *b, *c;

	double factor = -((double)k) / log(1.0 - pow(fp_threshold, 1.0/((double)k)));
	uint32_t bloom_size = factor * 2e6;

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;
		bloom_size = dpbf_mem[idx - 1];

		a = new StandardBloomFilter(fp_threshold, bloom_size, k);	
		b = new StandardBloomFilter(fp_threshold, bloom_size, k);

		// Insert common elements in both a and b
		for(uint32_t i=0; i < num_elements_in_intersection; i++) {
			a->sbf_insert(reduced_namespace[i]);
			b->sbf_insert(reduced_namespace[i]);
		}

		uint32_t set_size = plot_points[idx] / 2;
		double avg_intersection_fp = 0.0;

		for(uint32_t i = num_elements_in_intersection; i < num_elements_in_intersection + set_size; i++) {
			a->sbf_insert(reduced_namespace[i]);
		}

		for(uint32_t i = num_elements_in_intersection + set_size; i < num_elements_in_intersection + (2 * set_size); i++) {
			b->sbf_insert(reduced_namespace[i]);
		}

		//Find intersection
		c = new StandardBloomFilter(fp_threshold, bloom_size, k);
		(*c) = (*a) & (*b);

		// Calculate fp_rate
		uint64_t positives = 0;
		for(uint32_t i=0; i < reduced_namespace_size; i++) {
			if(c->sbf_lookup(reduced_namespace[i])) {
				positives++;
			}
		}

		cout<<"positives:"<<positives<<endl;
		avg_intersection_fp = (double)(positives - num_elements_in_intersection) / (double)(reduced_namespace_size);
		cout<<"fp rate:"<<avg_intersection_fp<<endl;

		basic_tests_file<<plot_points[idx]<<" "<<avg_intersection_fp<<" "<<bloom_size<<endl;
		delete a;
		delete b;
		delete c;
	}
	basic_tests_file.close();
}

void union_test_dbf(double fp_threshold, uint64_t* reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t bloom_size) {
	ofstream basic_tests_file;
	basic_tests_file.open(UNION_DBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	DynamicBloomFilter *a, *b, *c;

	uint64_t union_memory;

	a = new DynamicBloomFilter(fp_threshold, bloom_size, k);	
	b = new DynamicBloomFilter(fp_threshold, bloom_size, k);

	// Insert common elements in both a and b
	for(uint32_t i=0; i < num_elements_in_intersection; i++) {
		a->dbf_insert(reduced_namespace[i]);
		b->dbf_insert(reduced_namespace[i]);
	}

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;

		uint32_t diff = plot_points[idx] - plot_points[idx - 1];
		uint32_t set_size = diff / 2;
		double avg_union_fp = 0.0;

		// Insert elements in a
	 	for(uint32_t i = previous_index; i < previous_index + set_size; i++){
			a->dbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		// Insert elements in b
		for(uint32_t i = previous_index; i < previous_index + set_size; i++) {
			b->dbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		//Find union
		c = new DynamicBloomFilter(fp_threshold, bloom_size, k);
		(*c) = (*a) | (*b);
		
		// // Calculate fp_rate
		// uint64_t positives = 0;
		// for(uint32_t i=0; i < reduced_namespace_size; i++) {
		// 	if(c->dbf_lookup(reduced_namespace[i])) {
		// 		positives++;
		// 	}
		// }

		// cout<<"positives:"<<positives<<endl;
		// avg_union_fp = (double)(positives - (plot_points[idx] + num_elements_in_intersection)) / (double)(reduced_namespace_size);
		// cout<<"fp rate:"<<avg_union_fp<<endl;
		avg_union_fp = 0;

		// Memory
		union_memory = c->sbf_vector.size() * (c->sbf_num_bits/8) + sizeof(DynamicBloomFilter);			// in bytes

		basic_tests_file<<plot_points[idx]<<" "<<avg_union_fp<<" "<<union_memory<<endl;
		delete c;
	}
	basic_tests_file.close();
}

void intersection_test_dbf(double fp_threshold, uint64_t* reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t bloom_size) {
	ofstream basic_tests_file;
	basic_tests_file.open(INTERSECTION_DBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	DynamicBloomFilter *a, *b, *c;

	uint64_t intersection_memory;

	a = new DynamicBloomFilter(fp_threshold, bloom_size, k);	
	b = new DynamicBloomFilter(fp_threshold, bloom_size, k);

	// Insert common elements in both a and b
	for(uint32_t i=0; i < num_elements_in_intersection; i++) {
		a->dbf_insert(reduced_namespace[i]);
		b->dbf_insert(reduced_namespace[i]);
	}

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;

		uint32_t diff = plot_points[idx] - plot_points[idx - 1];
		uint32_t set_size = diff / 2;
		double avg_intersection_fp = 0.0;

		// Insert elements in a
	 	for(uint32_t i = previous_index; i < previous_index + set_size; i++){
			a->dbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		// Insert elements in b
		for(uint32_t i = previous_index; i < previous_index + set_size; i++) {
			b->dbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		//Find union
		c = new DynamicBloomFilter(fp_threshold, bloom_size, k);
		(*c) = (*a) & (*b);
		
		// // Calculate fp_rate
		// uint64_t positives = 0;
		// for(uint32_t i=0; i < reduced_namespace_size; i++) {
		// 	if(c->dbf_lookup(reduced_namespace[i])) {
		// 		positives++;
		// 	}
		// }

		// cout<<"positives:"<<positives<<endl;
		// avg_intersection_fp = (double)(positives - num_elements_in_intersection) / (double)(reduced_namespace_size);
		// cout<<"fp rate:"<<avg_intersection_fp<<endl;
		avg_intersection_fp = 0;

		// Memory
		intersection_memory = c->sbf_vector.size() * (c->sbf_num_bits/8) + sizeof(DynamicBloomFilter);			// in bytes

		basic_tests_file<<plot_points[idx]<<" "<<avg_intersection_fp<<" "<<intersection_memory<<endl;
		delete c;
	}
	basic_tests_file.close();
}

void union_test_dpbf(double fp_threshold, uint64_t* reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t num_partition, uint32_t fill_threshold, uint32_t bloom_size) {
	ofstream basic_tests_file;
	basic_tests_file.open(UNION_DPBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	DynamicPartitionBloomFilter *a, *b, *c;

	uint32_t query_set_size = 1e6;
	uint64_t union_memory;

	a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);	
	b = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);

	// Insert common elements in both a and b
	for(uint32_t i=0; i < num_elements_in_intersection; i++) {
		a->dpbf_insert(reduced_namespace[i]);
		b->dpbf_insert(reduced_namespace[i]);
	}

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;

		uint32_t diff = plot_points[idx] - plot_points[idx - 1];
		uint32_t set_size = diff / 2;
		double avg_union_fp = 0.0;

		// Insert elements in a
	 	for(uint32_t i = previous_index; i < previous_index + set_size; i++){
			a->dpbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		// Insert elements in b
		for(uint32_t i = previous_index; i < previous_index + set_size; i++) {
			b->dpbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		//Find union
		c = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);
		(*c) = (*a) | (*b);
		
		// Calculate fp_rate
		uint64_t positives = 0;
		for(uint32_t i=0; i < reduced_namespace_size; i++) {
			if(c->dpbf_lookup(reduced_namespace[i])) {
				positives++;
			}
		}

		cout<<"positives:"<<positives<<endl;
		avg_union_fp = (double)(positives - (plot_points[idx] + num_elements_in_intersection)) / (double)(reduced_namespace_size);
		cout<<"fp rate:"<<avg_union_fp<<endl;
		
		// Memory
		uint64_t size_non_leaf = sizeof(b_node);
		uint64_t size_leaf = sizeof(b_node) + c->bloom_size/8;
		uint64_t size_nodesUsed = c->myMap->size() * 4;
		uint64_t memory_snapshot = count_non_leaves(c->snapshot) * size_non_leaf + count_leaves(c->snapshot) * size_leaf;
		uint64_t memory_map = c->myMap->size() * (4 + c->bloom_size/8);	// size of key = 4, size of sbf = bloom_size/8
		union_memory = memory_map + memory_snapshot + size_nodesUsed;	// in bytes
		

		basic_tests_file<<plot_points[idx]<<" "<<avg_union_fp<<" "<<union_memory<<" "<<memory_map<<" "<<memory_snapshot<<endl;
		delete c;
	}
	basic_tests_file.close();
}

void intersection_test_dpbf(double fp_threshold, uint64_t* reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, uint32_t num_partition, uint32_t fill_threshold, uint32_t bloom_size) {
	ofstream basic_tests_file;
	basic_tests_file.open(INTERSECTION_DPBF_FILENAME);

	int plot_points[8] = {0,1000,2000,6000,10000,20000,60000,100000};
	int num_elements_in_intersection = 5000, previous_index = 5000, diff;
	DynamicPartitionBloomFilter *a, *b, *c;

	uint32_t query_set_size = 1e6;
	uint64_t intersection_memory;

	a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);	
	b = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);

	// Insert common elements in both a and b
	for(uint32_t i=0; i < num_elements_in_intersection; i++) {
		a->dpbf_insert(reduced_namespace[i]);
		b->dpbf_insert(reduced_namespace[i]);
	}

	for(uint32_t idx = 1; idx <= 7; idx++) {
		cout<<"idx:"<<idx<<endl;

		uint32_t diff = plot_points[idx] - plot_points[idx - 1];
		uint32_t set_size = diff / 2;
		double avg_intersection_fp = 0.0;

		// Insert elements in a
	 	for(uint32_t i = previous_index; i < previous_index + set_size; i++){
			a->dpbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		// Insert elements in b
		for(uint32_t i = previous_index; i < previous_index + set_size; i++) {
			b->dpbf_insert(reduced_namespace[i]);
		}

		previous_index += set_size;

		//Find intersection
		c = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);
		(*c) = (*a) & (*b);
		
		// // Calculate fp_rate
		// uint64_t positives = 0;
		// for(uint32_t i=0; i < reduced_namespace_size; i++) {
		// 	if(c->dpbf_lookup(reduced_namespace[i])) {
		// 		positives++;
		// 	}
		// }

		// cout<<"positives:"<<positives<<endl;
		// avg_intersection_fp = (double)(positives - num_elements_in_intersection) / (double)(reduced_namespace_size);
		// cout<<"fp rate:"<<avg_intersection_fp<<endl;
		avg_intersection_fp = 0;

		// Memory
		uint64_t size_non_leaf = sizeof(b_node);
		uint64_t size_leaf = sizeof(b_node) + c->bloom_size/8;
		uint64_t size_nodesUsed = c->myMap->size() * 4;
		uint64_t memory_snapshot = count_non_leaves(c->snapshot) * size_non_leaf + count_leaves(c->snapshot) * size_leaf;
		uint64_t memory_map = c->myMap->size() * (4 + c->bloom_size/8);		// size of key = 4, size of sbf = bloom_size/8
		intersection_memory = memory_map + memory_snapshot + size_nodesUsed;// in bytes

		basic_tests_file<<plot_points[idx]<<" "<<avg_intersection_fp<<" "<<intersection_memory<<" "<<memory_map<<" "<<memory_snapshot<<endl;
		delete c;
	}
	basic_tests_file.close();
}

void compression_ratio_test(uint64_t* userIDs) {
	uint32_t plot_points[18] = {0,10,20,60,100,200,600,1000,2000,6000,10000,20000,60000,100000,200000,600000,1000000,2000000};

	// DPBF parameters
	DynamicPartitionBloomFilter *a;

	ofstream compression_ratio_file;

	uint32_t c_list[] = {4, 6, 10};
	uint32_t k_list[] = {6, 6, 6};
	double fp_list[] = {1e-3, 1e-2, 1e-1};


	for(uint32_t param_idx = 0; param_idx < 3; param_idx++) {
		// Initialize parameters
		int C = c_list[param_idx];
		uint32_t k = k_list[param_idx];
		double fp_threshold = fp_list[param_idx];
		cout<<"C : "<<C<<endl;

		for(uint32_t num_partition = 25; num_partition <= 34; num_partition++) {
			uint32_t fill_threshold = pow(2, 34 - num_partition);
			uint32_t bloom_size = (64 * fill_threshold) / C;	
			cout<<"D : "<<num_partition<<endl;	

			// Open file
			string fileName = "../out/mem_ratio_results/compression_ratio_C";
			fileName.append(to_string(C));
			fileName.append("_D");
			fileName.append(to_string(num_partition));
			fileName.append(".txt");
			compression_ratio_file.open(fileName);
	
			for(uint32_t idx = 1; idx < 18; idx++) {
				cout<<"idx:"<<idx<<endl;
				uint32_t set_size = plot_points[idx];

				a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);

				// Insert elements
				for(uint32_t i=0; i<set_size; i++) {
					a->dpbf_insert(userIDs[i]);
				}

				// Update snapshot
				a->snapshot = a->dpbf_node_update(a->snapshot, 0, 0);

				uint64_t num_leaves = count_leaves(a->snapshot);
				double mem_ratio = (double)(set_size * 64) / (num_leaves * bloom_size);
				double num_ratio = (double)set_size / num_leaves;
				num_ratio = num_ratio / fill_threshold;

				compression_ratio_file<<idx<<" "<<set_size<<" "<<mem_ratio<<" "<<num_ratio<<endl;

				// Free Memory	
				a->dpbf_free();
			}

			// Close file
			compression_ratio_file.close();
		}
	}
}

void parameter_selection_test(uint64_t *userIDs, uint64_t *reduced_namespace, uint32_t reduced_namespace_size, uint32_t k, double fp_threshold, int C) {
	uint32_t plot_points[3] = {10000,100000,1000000};

	// DPBF parameters
	DynamicPartitionBloomFilter *a;

	uint32_t query_set_size = 1e6;
	clock_t start, end;
	double construction_time, insertion_time, updation_time, query_time, false_positive_rate;
	uint64_t memory_snapshot, memory_map, memory_total;

	ofstream parameter_selection_test_file;

	for(uint32_t idx = 0; idx < 3; idx++) {
		cout<<"idx:"<<idx<<endl;
		uint32_t set_size = plot_points[idx];

		// Open file
		string fileName = "../out/parameter_selection_";
		fileName.append(to_string(set_size));
		fileName.append(".txt");
		parameter_selection_test_file.open(fileName);

		for(uint32_t num_partition = 24; num_partition <= 34; num_partition++) {
			uint32_t fill_threshold = pow(2, 34 - num_partition);
			uint32_t bloom_size = (64 * fill_threshold) / C;

			// Construction time
			start = clock();
			a = new DynamicPartitionBloomFilter(bloom_size, k, num_partition, fill_threshold, fp_threshold);
			end = clock();
			construction_time = (double)(end - start)/CLOCKS_PER_SEC;

			// Insertion time
			start = clock();
			for(uint32_t i=0; i<set_size; i++) {
				a->dpbf_insert(userIDs[i]);
			}
			end = clock();
			insertion_time = (double)(end - start)/CLOCKS_PER_SEC;

			// Updation time
			start = clock();
			a->snapshot = a->dpbf_node_update(a->snapshot, 0, 0);
			end = clock();
			updation_time = (double)(end - start)/CLOCKS_PER_SEC;

			// Query time
			start = clock();
			for(uint32_t i=0; i<query_set_size; i++) {
				a->dpbf_lookup(reduced_namespace[i]);
			}
			end = clock();
			query_time = (double)(end - start)/CLOCKS_PER_SEC;

			// Memory
			uint64_t size_non_leaf = sizeof(b_node);
			uint64_t size_leaf = sizeof(b_node) + a->bloom_size/8;
			uint64_t size_nodesUsed = a->myMap->size() * 4;
			memory_snapshot = count_non_leaves(a->snapshot) * size_non_leaf + count_leaves(a->snapshot) * size_leaf;
			memory_map = a->myMap->size() * (4 + a->bloom_size/8);			// size of key = 4, size of sbf = bloom_size/8
			memory_total = memory_map + memory_snapshot + size_nodesUsed;	// in bytes

			// False positive rate
			uint64_t positives = 0;
			for(uint64_t i=0; i<reduced_namespace_size; i++) {
				if(a->dpbf_lookup(reduced_namespace[i]))
					positives++;
			}
			false_positive_rate = (double)(positives - set_size)/reduced_namespace_size;

			parameter_selection_test_file<<num_partition<<" "<<construction_time<<" "<<insertion_time<<" "<<updation_time<<" "<<query_time<<" "<<false_positive_rate<<" "<<memory_snapshot<<" "<<memory_map<<" "<<memory_total<<endl;

			// Free Memory	
			a->dpbf_free();
		}
		parameter_selection_test_file.close();
	}
}


// New Structure
int main() {

	// Declare and initialize output filenames
	BASIC_TESTS_DPBF_FILENAME = "../out/bt_dpbf.txt";
	BASIC_TESTS_SBF_FILENAME = "../out/bt_sbf.txt";
	BASIC_TESTS_DBF_FILENAME = "../out/bt_dbf.txt";
	UNION_SBF_FILENAME = "../out/union_sbf.txt";
	UNION_DBF_FILENAME = "../out/union_dbf.txt";
	UNION_DPBF_FILENAME = "../out/union_dpbf.txt";
	INTERSECTION_SBF_FILENAME = "../out/intersection_sbf.txt";
	INTERSECTION_DBF_FILENAME = "../out/intersection_dbf.txt";
	INTERSECTION_DPBF_FILENAME = "../out/intersection_dpbf.txt";

	// Read twitter data
	uint64_t* userIDs = read_data_from_file("../../../../../repos/dynamic-partition-bloom/data/twitter_data/unique_userIDs.txt", 3008497);	
	uint64_t* reduced_namespace = read_data_from_file("../../../../../repos/dynamic-partition-bloom/data/twitter_data/random_sample_new.txt", 1e8);

	// Initialize parameters
	int C = 6;
	uint32_t k = 6;
	double fp_threshold = 1e-2;
	uint32_t num_partition = 30;
	uint32_t fill_threshold = pow(2, 34 - num_partition);
	uint32_t bloom_size = (64 * fill_threshold) / C;

	// Run tests
	cout<<"Starting SBF tests..\n";
	basic_tests_sbf(userIDs, reduced_namespace, 1e8, k, fp_threshold);
	cout<<"Starting DBF tests..\n";
	basic_tests_dbf(userIDs, reduced_namespace, 1e8, k, bloom_size, fp_threshold);
	cout<<"Starting DBPF tests..\n";
	basic_tests_dpbf(userIDs, reduced_namespace, 1e8, k, num_partition, fill_threshold, bloom_size, fp_threshold);
	cout<<"Starting ratio tests..\n";
	dpbf_ratio_test(k, num_partition, fill_threshold, bloom_size, fp_threshold);
	cout<<"Starting SBF union test..\n";
	union_test_sbf(fp_threshold, reduced_namespace, 1e8, k);
	cout<<"Starting SBF intersection test..\n";
	intersection_test_sbf(fp_threshold, reduced_namespace, 1e8, k);
	cout<<"Starting DBF union test..\n";
	union_test_dbf(fp_threshold, reduced_namespace, 1e8, k, bloom_size);
	cout<<"Starting DBF intersection test..\n";
	intersection_test_dbf(fp_threshold, reduced_namespace, 1e8, k, bloom_size);
	cout<<"Starting DPBF union test..\n";
	union_test_dpbf(fp_threshold, reduced_namespace, 1e8, k, num_partition, fill_threshold, bloom_size);
	cout<<"Starting DPBF intersection test..\n";
	intersection_test_dpbf(fp_threshold, reduced_namespace, 1e8, k, num_partition, fill_threshold, bloom_size);
	cout<<"Starting compression factor test..\n";
	compression_ratio_test(userIDs);
	cout<<"Starting parameter selection test..\n";
	parameter_selection_test(userIDs, reduced_namespace, 1e8, k, fp_threshold, C);

	return 0;
}
