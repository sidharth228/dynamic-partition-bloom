#ifndef _SNAPSHOT_H_
#define _SNAPSHOT_H_

#include "partition_hash.h"
#include "StandardBloomFilter.h"

using namespace std;

/***
	Class for storing the CPBPT of DPBF. Must contain the following functions - 
		-	count_leaves(b_node *root)
		-	count_non_leaves(b_node *root)
		-	snapshot_lookup(uint64_t val, b_node *root)
***/

// Node for the tree representing the snapshot
struct b_node {
	StandardBloomFilter *bf;
	b_node *left;
	b_node *right;
	bool is_dirty, is_sorted;
	vector<uint32_t> *nodesUsed;
};

/**
	Initialzes the snapshot (whch is basically a tree of b_nodes)
	@param a : The root of the snapshot which is initialized
**/
void snapshot_node_init(b_node *a) {
	a->bf = NULL;
	a->left = NULL;
	a->right = NULL;
	a->is_dirty = false;
	a->is_sorted = true;
	a->nodesUsed = new vector<uint32_t>();
}

/**
	Frees a snapshot by recursively freeing it's children first
	@param curr_node : The curr_node in the recursion. It's children are freed first and then the node itself	
**/
void snapshot_free(b_node *curr_node) {
	if(curr_node == NULL) {
		return;
	} else {

		// Free it's children
		snapshot_free(curr_node->left);
		snapshot_free(curr_node->right);
	
		// Free the SBF
		if(curr_node->bf != NULL) {
			delete curr_node->bf;
		}	

		delete curr_node->nodesUsed;

		// Free the node itself
		delete curr_node;
	}
}

/**
	Checks if a b_node is a leaf or not
	@param node 	: The node which is to be checked
	@return bool 	: True of False depending on whether it's leaf or not
**/
bool is_leaf(b_node *node) {
	if(node->left == NULL && node->right == NULL) {
		return true;			
	} else {
		return false;			
	}
}

/**
	Checks if an element is already in the snapshot or not
	@param val 			: The value which is to be checked
	@param root 		: The snapshot in which the value is to be checked 
	@param num_hashes	: The number of hash functions used in SBFs
	@param num_bits 	: The size of SBF of leaf nodes
	@return bool 		: True or False depending on whether val is present or not
**/
bool snapshot_lookup(uint64_t val, b_node* root, uint32_t num_hashes, uint32_t num_bits) {
	b_node *curr_node = root;
	uint32_t curr_level = 0;
	while(true) {
		if (curr_node->bf != NULL) {
			return curr_node->bf->sbf_lookup(val);
		} else {
			if(partition_hash(val, curr_level) == 0) {
				curr_node = curr_node->left;
			} else {
				curr_node = curr_node->right;
			}
		}
		curr_level++;
	}
}

/**
	Counts the number of leaves in the snapshot
	@param node 	: The root of the snapshot
	@return count 	: The number of leaves 
**/
uint32_t count_leaves (b_node *node) {
	if(is_leaf(node)) {
		return 1;
	} else if (node->left == NULL) {
		// printf("Caution:: Binary tree is not a full binary tree\n");
		return count_leaves(node->right);
	} else if (node->right == NULL) {
		// printf("Caution:: Binary tree is not a full binary tree\n");
		return count_leaves(node->left);
	} else {
		return count_leaves(node->left) + count_leaves(node->right);
	}
}

/**
	Counts the number of non-leaves in the snapshot
	@param node 	: The root of the snapshot
	@return count 	: The number of non-leaves 
**/
uint32_t count_non_leaves(b_node *node) {
	if(is_leaf(node)) {
		return 0;
	} else if (node->left == NULL) {
		// printf("Caution:: Binary tree is not a full binary tree\n");
		return 1 + count_non_leaves(node->right);
	} else if (node->right==NULL) {
		// printf("Caution:: Binary tree is not a full binary tree\n");
		return 1 + count_non_leaves(node->left);
	} else {
		return 1 + count_non_leaves(node->left) + count_non_leaves(node->right);
	}
}

/**
	Sets the dirty-bit in the corresponding leaf of the snapshot and also adds the index to the nodesUsed.
	In case the element can be added directly to CPBPT, it is done without setting the leaf dirty.
	@param val 		: The value which is currently being inserted in DPBF for which the dirty-bit is set
	@param root 	: The root of the snapshot
	@param index	: The index of the leaf node in case it is to be inserted in nodesUsed vector of root
**/
void set_dirty_bit(uint64_t val, b_node *root, uint32_t fill_threshold, uint32_t fp, uint32_t bloom_size, uint32_t num_hash, bool is_delete, uint32_t index = -1) {
	b_node *curr_node = root;
	b_node *parent_node = NULL;
	uint32_t curr_level = 0;
	uint32_t direction;
	while(!is_leaf(curr_node)) {
		direction = partition_hash(val, curr_level);
		parent_node = curr_node;
		if(direction == 0) {
			// Move left
			curr_node = curr_node->left;
		} else if(direction == 1) {
			// Move right
			curr_node = curr_node->right;
		} else {
			// Error
			printf("Partition functions exhausted (while setting dirty bit in snapshot)!\n\n\n");
			exit(0);
		}
		curr_level++;
		if(curr_node == NULL) {		
			// We've reached a null child of an internal node		
			// Create a new node		
			curr_node = new b_node();		
			snapshot_node_init(curr_node);		
			curr_node->bf = new StandardBloomFilter(fp, bloom_size, num_hash);		
			if(direction == 0) {		
				parent_node->left = curr_node;		
			} else {		
				parent_node->right = curr_node;		
			}		
			break;		
		}
	}
	
	// We've reached a leaf node
	// Insert the element in CPBPT if possible
	if(is_delete) {
		// Delete operation so directly set dirty bit
		curr_node->is_dirty = true;
	} else if(curr_node->bf->num_elements < fill_threshold) {		
		curr_node->bf->sbf_insert(val);		
	} else {		
		// Else set dirty bit		
		curr_node->is_dirty = true;				
	}

	// Push nodes in nodesUsed vector
	if(index != -1) {
		curr_node->is_sorted = false;

		// Add the index to nodesUsed
		curr_node->nodesUsed->push_back(index);	
	}
}

#endif