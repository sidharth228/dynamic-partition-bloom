#ifndef _DPBF_H_
#define _DPBF_H_

#include <iostream>
#include <unordered_map>
#include <algorithm>
#include "snapshot.h"

using namespace std;

/**
	Helper structures used in the update function for DPBF.
**/
		
struct int_ll_node {
	uint32_t data;
	int_ll_node* next;
};

struct list_node { 
	b_node *node;
	int_ll_node *start, *curr;
	uint32_t index, numElems;
	list_node* next;
	bool is_leaf;
};

/***
	Class for DPBF. DPBF consists of a CPBPT and a Hash Map. The class must have the following functions -

	Insertion and lookup : 
		-	dpbf_insert(uint64_t val)
		-	dpbf_delete(uint64_t val)
		-	dpbf_lookup(uint64_t val)

	Union and intersection operators are defined accordingly.
***/

class DynamicPartitionBloomFilter {
public:
	uint32_t bloom_size;									// m
	uint32_t num_hash;										// k
	uint32_t num_partition;									// maximum height of snapshot
	double fp;												// fp rate to be maintained
	uint32_t fill_threshold;								// n
	unordered_map<uint32_t, StandardBloomFilter*> *myMap;	// map of dpbf
	b_node *snapshot;										// snapshot of dpbf

	DynamicPartitionBloomFilter(uint32_t bloom_size, uint32_t num_hash, uint32_t num_partition, uint32_t fill_threshold, double fp);

	void dpbf_free();
	void dpbf_insert(uint64_t val);
	void dpbf_delete(uint64_t val);
	bool dpbf_lookup(uint64_t val);
	b_node* dpbf_node_update(b_node *root, uint32_t curr_value, uint32_t curr_height);
};

/**
	Constructor for DPBF class.
	@param bloom_size 	: The size of SBFs of leaf node in bits
	@param num_hash 	: The number of hash functions used by SBFs of leaf nodes
	@param num_partition: The maximum height to which our snapshot can grow
	@param fp			: The FP threshold to be maintained by our structure
**/
DynamicPartitionBloomFilter::DynamicPartitionBloomFilter (uint32_t bloom_size, uint32_t num_hash, uint32_t num_partition, uint32_t fill_threshold, double fp) {
	this->myMap = new unordered_map<uint32_t, StandardBloomFilter*> ();
	this->bloom_size = bloom_size;
	this->num_hash = num_hash;
	this->num_partition = num_partition;
	this->fp = fp;
	this->fill_threshold = fill_threshold;
	this->snapshot = new b_node();
	snapshot_node_init(this->snapshot);
	this->snapshot->bf = new StandardBloomFilter(fp, bloom_size, num_hash);
}

/**
	Function to free memory for DPBF
**/
void DynamicPartitionBloomFilter::dpbf_free() {
	snapshot_free(this->snapshot);
	unordered_map<uint32_t, StandardBloomFilter*>::iterator it;
	for(it = this->myMap->begin(); it != this->myMap->end(); it++) {
		if(it->second != NULL) {
			delete it->second;
		}
	}
	delete this->myMap;
	delete this;	
}

/**
	Function to insert in DPBF.
	@param val : The value to be inserted
**/
void DynamicPartitionBloomFilter::dpbf_insert(uint64_t val) {
	uint32_t index = 0;

	// Find the index of the bloom filter in the linked list
	for(uint32_t curr_level = 0; curr_level < this->num_partition; curr_level++) {
		index = index * 2 + partition_hash(val, curr_level);
	}

	unordered_map<uint32_t, StandardBloomFilter*>::iterator it = this->myMap->find(index);

	if(it != this->myMap->end()) {
		// Insert in sbf
		if(!it->second->sbf_lookup(val)) {
			// If it's not a duplicate
			it->second->sbf_insert(val);
			set_dirty_bit(val, this->snapshot, this->fill_threshold, this->fp, this->bloom_size, this->num_hash, false);			
		}
	} else {
		// create a new node and insert in map
		StandardBloomFilter *new_sbf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);
		new_sbf->sbf_insert(val);

		this->myMap->insert(pair<uint32_t, StandardBloomFilter*>(index, new_sbf));
		set_dirty_bit(val, this->snapshot, this->fill_threshold, this->fp, this->bloom_size, this->num_hash, false, index);
	}
}

/**
	Function to delete from DPBF.
	@param val : The value to be deleted
**/
void DynamicPartitionBloomFilter::dpbf_delete(uint64_t val) {
	uint32_t index = 0;

	// Find the index of the bloom filter in the linked list
	for(uint32_t curr_level = 0; curr_level < this->num_partition; curr_level++) {
		index = index * 2 + partition_hash(val, curr_level);
	}

	unordered_map<uint32_t, StandardBloomFilter*>::iterator it = this->myMap->find(index);

	if(it != this->myMap->end()) {
		// Insert in sbf
		it->second->sbf_delete(val);
		set_dirty_bit(val, this->snapshot, this->fill_threshold, this->fp, this->bloom_size, this->num_hash, true);			
	} 
}

/**
	Helper function to update a node in snapshot by reconstructing the required subtree from the HashMap.
	@param root 		: The node to be updated
	@param curr_value	: The index of the node in the tree (if all nodes were numbered with root being 0)
	@param curr_height 	: The level of the node
	@return b_node		: The updated node
**/
b_node* DynamicPartitionBloomFilter::dpbf_node_update(b_node *root, uint32_t curr_value, uint32_t curr_height) {
	uint32_t start, end;
	start = curr_value * pow(2, this->num_partition - curr_height);
	end = start + pow(2, this->num_partition - curr_height) - 1;
	uint32_t bottom_up_height = this->num_partition;

	list_node *start_node = NULL, *curr_node, *del_node;

	// Sort the indices
	if(!root->is_sorted) {
		sort(root->nodesUsed->begin(), root->nodesUsed->end());	
	}
	vector<uint32_t>::iterator it = root->nodesUsed->begin();

	uint32_t num_pbf_nodes = 0;

	while(it != root->nodesUsed->end()) {
		list_node* l_node = new list_node();
		l_node->index = *it;
		l_node->start = new int_ll_node;
		l_node->start->data = *it;
		l_node->start->next = NULL;
		unordered_map<uint32_t, StandardBloomFilter*>::iterator new_it = this->myMap->find(*it);
		l_node->numElems = new_it->second->num_elements;
		l_node->is_leaf = true;
		l_node->curr = l_node->start;
		l_node->next = NULL;

		if(start_node == NULL) {
			start_node = l_node;
			curr_node = l_node;
			num_pbf_nodes++;
		} else {
			curr_node->next = l_node;
			curr_node = curr_node->next;
			num_pbf_nodes++;
		}

		it++;
	}

	// Free current root
	snapshot_free(root);

	if(num_pbf_nodes == 0) {
		
		root = new b_node();
		snapshot_node_init(root);

		// Initialize the bloom filter of the b_node
		root->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

	} else if(num_pbf_nodes == 1) {

		root = new b_node();
		snapshot_node_init(root);

		// Initialize the bloom filter of the b_node
		root->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

		// Copy the bloom filter of the full tree in the one for temp node
		unordered_map<uint32_t, StandardBloomFilter*>::iterator new_it = this->myMap->find(start_node->start->data);
		root->bf->copy_sbf(new_it->second);		
		root->nodesUsed->push_back(start_node->start->data);

		delete start_node;
	} else {
		
		uint64_t l_index;
		while(num_pbf_nodes > 1) {
			curr_node = start_node;
			bottom_up_height --;

			while(curr_node != NULL) {

				l_index = curr_node->index;
				if(l_index % 2 == 0) {
					// Even
					if(curr_node->is_leaf) {
						// Leaf
						if(curr_node->next != NULL && (curr_node->next)->index == l_index + 1) {
							if ((curr_node->next->is_leaf) && (curr_node->numElems + curr_node->next->numElems) <= this->fill_threshold) {
								// Take union
								curr_node->numElems = curr_node->numElems + curr_node->next->numElems;
								del_node = curr_node->next;

								// Accumulate indices in int_ll
								curr_node->curr->next = (curr_node->next)->start;
								curr_node->curr = (curr_node->next)->curr;

								curr_node->next = (curr_node->next)->next;
								delete del_node;
								num_pbf_nodes--;
							} else {
								// Create parent node
								b_node *parent_node = new b_node();
								snapshot_node_init(parent_node);

								b_node *left_node = new b_node();
								snapshot_node_init(left_node);

								left_node->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

								unordered_map<uint32_t, StandardBloomFilter*>::iterator it;
		
								// Add indices in vector of curr_node
								while(curr_node->start != NULL) {
									left_node->nodesUsed->push_back(curr_node->start->data);
									it = this->myMap->find(curr_node->start->data);
									// cout<<"Taking union of two sbf's :-\n";
									// print_sbf(left_node->bf);
									// print_sbf(it->second);
									*(left_node->bf) |= *(it->second);
									// left_node->bf->num_elements = left_node->bf->estimated_population(this->bloom_size, this->num_hash);
									// cout<<"Final sbf:-\n";
									// print_sbf(left_node->bf);
									// union_sbf(left_node->bf, it->second, left_node->bf);
									int_ll_node* temp_del_node = curr_node->start;
									curr_node->start = curr_node->start->next;
									delete temp_del_node;
								}

								left_node->bf->num_elements = curr_node->numElems;

								parent_node->left = left_node;

								if(curr_node->next->is_leaf) {
									b_node *right_node = new b_node();
									snapshot_node_init(right_node);

									right_node->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

									// Add indices in vector of curr_node
									while(curr_node->next->start != NULL) {
										right_node->nodesUsed->push_back(curr_node->next->start->data);
										it = this->myMap->find(curr_node->next->start->data);
										// cout<<"Taking union of two sbf's :-\n";
										// print_sbf(right_node->bf);
										// print_sbf(it->second);
										*(right_node->bf) |= *(it->second);
										// right_node->bf->num_elements = right_node->bf->estimated_population(this->bloom_size, this->num_hash);
										// cout<<"Final sbf:-\n";
										// print_sbf(right_node->bf);
										// union_sbf(right_node->bf, it->second, right_node->bf);
										int_ll_node* temp_del_node = curr_node->next->start;
										curr_node->next->start = curr_node->next->start->next;
										delete temp_del_node;
									}

									right_node->bf->num_elements = curr_node->next->numElems;
									parent_node->right = right_node;

								} else {
									parent_node->right = curr_node->next->node;									
								}

								curr_node->is_leaf = false;
								curr_node->node = parent_node;
								del_node = curr_node->next;
								curr_node->next = curr_node->next->next;
								delete del_node;
								num_pbf_nodes--;
							}
						} 
					} else { 
						// Not leaf
						if(curr_node->next != NULL && curr_node->next->index == l_index + 1) {
							// Create parent node
							b_node *parent_node = new b_node;
							snapshot_node_init(parent_node);

							parent_node->left = curr_node->node;

							if(curr_node->next->is_leaf) {
								b_node *right_node = new b_node;
								snapshot_node_init(right_node);

								right_node->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

								unordered_map<uint32_t, StandardBloomFilter*>::iterator it;

								// Add indices in vector of curr_node
								while(curr_node->next->start != NULL) {
									right_node->nodesUsed->push_back(curr_node->next->start->data);
									it = this->myMap->find(curr_node->next->start->data);
									// cout<<"Taking union of two sbf's :-\n";
									// print_sbf(right_node->bf);
									// print_sbf(it->second);
									*(right_node->bf) |= *(it->second);
									// right_node->bf->num_elements = right_node->bf->estimated_population(this->bloom_size, this->num_hash);
									// cout<<"Final sbf:-\n";
									// print_sbf(right_node->bf);
									// union_sbf(right_node->bf, it->second, right_node->bf);
									int_ll_node* temp_del_node = curr_node->next->start;
									curr_node->next->start = curr_node->next->start->next;
									delete temp_del_node;
								}

								right_node->bf->num_elements = curr_node->next->numElems;
								parent_node->right = right_node;
							} else {
								parent_node->right = curr_node->next->node;
							}

							curr_node->node = parent_node;
							del_node = curr_node->next;
							curr_node->next = (curr_node->next)->next;
							delete del_node;
							num_pbf_nodes--;
						
						} else {
							//Right child of internal node is null
							b_node *parent_node = new b_node();
							snapshot_node_init(parent_node);

							parent_node->left = curr_node->node;

							curr_node->node = parent_node;					
						}
					}
				} else {
					// Odd
					if(!curr_node->is_leaf) {
						// Not Leaf
						// Left child of parent node is null
						b_node *parent_node = new b_node();
						snapshot_node_init(parent_node);

						parent_node->right = curr_node->node;

						curr_node->node = parent_node;
					} 
				}	

				// Update index
				curr_node->index = l_index / 2;
				curr_node = curr_node->next;
			}
		}

		if(start_node->is_leaf) {
			start_node->node = new b_node();
			snapshot_node_init(start_node->node);

			start_node->node->bf = new StandardBloomFilter(this->fp, this->bloom_size, this->num_hash);

			// Add indices in nodesUsed vector 
			while(start_node->start != NULL) {
				start_node->node->nodesUsed->push_back(start_node->start->data);
				unordered_map<uint32_t, StandardBloomFilter*>::iterator it = this->myMap->find(start_node->start->data);
				// cout<<"Taking union of two sbf's :-\n";
				// print_sbf(start_node->node->bf);
				// print_sbf(it->second);
				*(start_node->node->bf) |= *(it->second);
				// start_node->node->bf->num_elements = start_node->node->bf->estimated_population(this->bloom_size, this->num_hash);
				// cout<<"Final sbf:-\n";
				// print_sbf(start_node->node->bf);
				int_ll_node* temp_del_node = start_node->start;
				start_node->start = start_node->start->next;
				delete temp_del_node;
			}
		}

		root = start_node->node;

		// In case only one node remains but we still haven't reached the height of the dirty-leaf node
		if(!start_node->is_leaf) {
			while(bottom_up_height > curr_height) {
				bottom_up_height --;
				l_index = start_node->index;

				if(l_index % 2 == 0) {
					// Right child of parent node is null
					b_node *parent_node = new b_node();
					snapshot_node_init(parent_node);

					parent_node->left = start_node->node;

					start_node->node = parent_node;
				} else {
					// Left child of parent node is null
					b_node *parent_node = new b_node();
					snapshot_node_init(parent_node);

					parent_node->right = start_node->node;

					start_node->node = parent_node;
				}
			}
		}
		root = start_node->node;			
	}
	return root;
}

/**
	Function to check if an element is in DPBF.
	@param val : The value to be checked

**/
bool DynamicPartitionBloomFilter::dpbf_lookup(uint64_t val) {
	b_node *curr_node = this->snapshot, *parent_node = NULL;
	uint32_t curr_level = 0, curr_value = 0, curr_direction = 0;

	// Find the value in the snapshot
	while(true) {
		// Child of internal nodes may be null
		if(curr_node == NULL) {
			return false;
		} else if (is_leaf(curr_node)) {
			// If dirty-bit is set, then update (lazy propagate)
			if(curr_node->is_dirty) {
				// printf("dirty leaf\n");
				if(parent_node == NULL) {
					this->snapshot = this->dpbf_node_update(this->snapshot, curr_value, curr_level);
					curr_node = this->snapshot;
				} else {
					if(curr_direction == 0) {
						parent_node->left = this->dpbf_node_update(parent_node->left, curr_value, curr_level);
						curr_node = parent_node->left;
					} else {
						parent_node->right = this->dpbf_node_update(parent_node->right, curr_value, curr_level);
						curr_node = parent_node->right;
					}
				}

				// To start at the same node in the next iteration
				curr_level--;				

			} else {
				// Dirty bit is not set for leaf node, so check if the element is present in sbf of that leaf node
				return curr_node->bf->sbf_lookup(val);
			}
		} else {
			// Continue search
			parent_node = curr_node;
			if(partition_hash(val, curr_level) == 0) {
				curr_node = curr_node->left;
				curr_value = curr_value * 2;
				curr_direction = 0;
			} else {
				curr_node = curr_node->right;
				curr_value = curr_value * 2 + 1;
				curr_direction = 1;
			}
		}

		curr_level++;
	}

}

#endif
