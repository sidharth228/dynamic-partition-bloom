#ifndef _STANDARD_BLOOM_FILTER_H
#define _STANDARD_BLOOM_FILTER_H

#include <cmath>
#include <functional>
#include <iostream>
#include <vector>
#include "MurmurHash3.cpp"

using namespace std;

/***
    Class for a standard bloom filter (SBF). Must define the following functions - 

    Insertion and lookup :
        -   sbf_insert(uint64_t val)
        -   sbf_delete(uint64_t val)
        -   sbf_lookup(uint64_t val)
    
    Union and intersection operators are defined as usual.
***/

class StandardBloomFilter {
public:
    static double fp;                                   // false positive rate threshold
    static uint32_t bloom_size;                         // m (in bits)
    static uint32_t num_hash;                           // k
    uint32_t num_elements;

    vector<uint8_t> bit_vector;                         // underlying counting_bloom_filter

    StandardBloomFilter(double, uint32_t, uint32_t);

    void sbf_insert(uint64_t);
    void sbf_delete(uint64_t);
    bool sbf_lookup(uint64_t);
    bool lookup_insert_helper(uint64_t, int);
    void copy_sbf(StandardBloomFilter*);

    StandardBloomFilter operator|(StandardBloomFilter& sbf);
    void operator|=(StandardBloomFilter& sbf);
};

double StandardBloomFilter::fp = 0;
uint32_t StandardBloomFilter::bloom_size = 0;
uint32_t StandardBloomFilter::num_hash = 0;

/**
    The constructor of our SBF class.
    @param fp           : The FP-threshold for SBF
    @param bloom_size   : The size of SBF in bits 
    @param num_hash     : The number of hash functions used by SBF
**/
StandardBloomFilter::StandardBloomFilter(double fp, uint32_t bloom_size, uint32_t num_hash) {
    this->bit_vector = vector<uint8_t>(bloom_size); // all 0's by default
    this->num_elements = 0;
    this->fp = fp;
    this->bloom_size = bloom_size;
    this->num_hash = num_hash;
}

/**
    A helper function used for insert and lookup. 
    @param x        : The value to be inserted/looked-up
    @param insert   : The case defining whether it's an insert or a lookup (true in case of insert)
    @return bool    : True or False depending on whether x was present or not in case of look-up
**/
bool StandardBloomFilter::lookup_insert_helper(uint64_t x, int case_) {
    uint64_t hash_digests[2];
    uint32_t hits = 0;

    // store the 128 bit output in two 64 bit variables
    MurmurHash3_x64_128(&x, 8, 0, hash_digests);

    for(uint64_t i = 0; i < this->num_hash; i++) {
        uint32_t pos = (hash_digests[0] % this->bloom_size + i*(hash_digests[1] % this->bloom_size)) % this->bloom_size;
        if(case_ == 0) {
            //Insert
            this->bit_vector[pos]++;
        } else if(case_ == 1) {
            //Delete
            this->bit_vector[pos]--;
        } else if(case_ == 2) {
            //Lookup
            if(this->bit_vector[pos] > 0) {
                hits++;
            }
        }
    }

    if(case_ == 0) {
        this->num_elements = this->num_elements + 1;
    } else if (case_ == 1) {
        this->num_elements = this->num_elements - 1;
    } else if (case_ == 2) {
        if (hits == this->num_hash) {
            return true;
        } else {
            return false;
        }
    }
}

/**
    Function to insert in SBF.
    @param x : Value to be inserted in SBF
**/
void StandardBloomFilter::sbf_insert(uint64_t x) {
    this->lookup_insert_helper(x, 0);
}

/**
    Functino to delete an element from SBF
    @param x : Value to be deleted
**/
void StandardBloomFilter::sbf_delete(uint64_t x) {
    if(this->sbf_lookup(x)) {
        this->lookup_insert_helper(x, 1);
    } else {
        cout<<"Element "<<x<<" does not exist\n";
    }
}

/**
    Function to check if an element is in SBF
    @param x : Value to be checked in SBF
**/
bool StandardBloomFilter::sbf_lookup(uint64_t x) {
    return this->lookup_insert_helper(x, 2);
}

/**
    Union operator for SBF. 
**/
StandardBloomFilter StandardBloomFilter::operator|(StandardBloomFilter& sbf) {
    StandardBloomFilter ret(this->fp, this->bloom_size, this->num_hash);
    for(uint32_t i = 0; i < this->bloom_size; i++) {
        ret.bit_vector[i] = this->bit_vector[i] + sbf.bit_vector[i];
    }
    ret.num_elements = this->num_elements + sbf.num_elements;
    return ret;
}

/**
    Another union operator.
**/
void StandardBloomFilter::operator|=(StandardBloomFilter& sbf) {
    for(uint32_t i = 0; i < this->bloom_size; i++) {
        this->bit_vector[i] += sbf.bit_vector[i];
    }
    this->num_elements += sbf.num_elements;
}

/**
    Function to make a copy of SBF. Maybe not required.    
 **/
void StandardBloomFilter::copy_sbf(StandardBloomFilter *a) {
    for(uint32_t i = 0; i < this->bloom_size; i++) {
        this->bit_vector[i] = a->bit_vector[i];
    }
    this->num_elements = a->num_elements;
}

#endif
