# Dynamic Partition Bloom Filters - *Insert-Delete*

This README explains how to use the code for DPBF to insert and delete elements from a DPBF. This assumes that you've already downloaded the required code on your local system and imported the required libraries.

## Initializing

First you must declare the struture and initialize it using the constructor. 
```
DynamicPartionBloomFilter *a;
a = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);	// (m, k, D, n_t, p)
```
The constructor parameters are the same as defined for DPBF with set operations.

## Insertion

To insert an element e, we use the following function 
```
a->dpbf_insert(e);
```
Here the element (e) inserted must be an integer with atmost 64 bits.

## Query

To check if an element e exists in the structure, we use the following function
```
a->dpbf_query(e);
```
This returns true or false based on whether the element was present or not.

## Delete

To delete an element e, we use the following function 
```
a->dpbf_delete(e);
```
## SBF - delete

For deletion in SBF, the respective function is sbf_delete(). An example is provided below 
```
\\SBF Example
StandardBloomFilter *b = new StandardBloomFilter(0.0001, 1000, 3);	// (p, m, k)
b->sbf_insert(6);
b->sbf_delete(6);
```
