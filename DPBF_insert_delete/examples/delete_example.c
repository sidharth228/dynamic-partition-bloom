#include <iostream>
#include <stdlib.h>
#include "../src/DynamicPartitionBloomFilter.h"

using namespace std;
int main() {

	DynamicPartitionBloomFilter *a;
	a = new DynamicPartitionBloomFilter(170, 3, 30, 16, 0.01);

	// Insert an element
	a->dpbf_insert(5);

	// Lookup to check if element exists
	if(a->dpbf_lookup(5)) {
		cout << "Element exists\n";
	}

	// Delete element
	a->dpbf_delete(5);

	// Lookup to see if element is deleted
	if(!a->dpbf_lookup(5)) {
		cout << "Element was deleted\n";
	}

	return 0;
}